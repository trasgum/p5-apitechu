### En Mockaroo añadir: password a los usuarios.
### Añadir operaciones a la API:
#### LoginPOST a /apitechu/v1/login

En el body un JSON tal que:

`{ "email" : "test@test.com", "password" : "1234" }`

- Buscar el usuario con email igual al enviado:
* Si no está el email -> NOK.
* Si está el email:
* Si el password es igual al enviado -> OK
* Si el password no es igual al enviado -> NOK
* Si es login correcto:Añadir al usuario una propiedad "logged" : true

- Persistir el cambio.
- Devolver la id de usuario.

Ejemplo de respuestas:Login incorrecto { "mensaje" : "Login incorrecto" }Login correcto { "mensaje" : "Login correcto", "idUsuario" : 1 }

#### LogoutPOST a /apitechu/v1/logout/En el body un JSON tal que:

`{ "id" : 1 }`

Tanto para logout correcto como incorrecto, devolver un mensaje informativo.En caso de logout correcto, persistir el cambio.Ejemplo de respuestas:
- Logout incorrecto
`{ "mensaje" : "logout incorrecto" }`

Logout correcto
`{ "mensaje" : "logout correcto" "idUsuario" : 1 }`
