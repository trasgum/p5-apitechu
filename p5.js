const express = require('express');

const app = express();
app.use(express.json());
const port = process.env.PORT || 3000;

app.listen(port);

console.log(`TEST API escuchando en el puerto ${port}`);

app.post('/apitechu/v1/login',
  (req, res) => {
    console.log('POST /apitechu/v1/login');
    console.log(`Body: ${JSON.stringify(req.body)}`);

    let login = false;
    let mensaje = '{ "mensaje" : "Login incorrecto" }';

    const users = require('./usuarios.json');
    const user = users.find(o => o.email === req.body.email);
    if (user && user.password === req.body.password && !user.logged) {
      console.log(`LOGIN - login ok ${req.body.email}`);
      login = true;
      mensaje = `{ "mensaje" : "Login correcto", "idUsuario" : ${user.id} }`;
      user.logged = true;
      users.splice(users.indexOf(user), 1);
      users.push(user);
      writeUserData2File(users);
    } else {
      console.log(`LOGIN - login ko ${req.body.email}`);
    }
    res.send(mensaje);
  });

app.post('/apitechu/v1/logout',
  (req, res) => {
    console.log('POST /apitechu/v1/logout');
    let mensaje = '{ "mensaje" : "logout incorrecto" }';
    const users = require('./usuarios.json');
    const user = users.find(o => o.id === req.body.id);
    if (user && user.logged) {
      console.log(`LOGOUT - logout ok ${req.body.id}`);
      mensaje = `{ "mensaje" : "logout correcto", "idUsuario" : ${user.id} }`;
      delete user.logged;
      users.splice(users.indexOf(user), 1);
      users.push(user);
      writeUserData2File(users);
    } else {
      console.log(`LOGOUT - logout ko ${req.body.id}`);
    }
    res.send(mensaje);
  });

app.get('/apitechu/v1/hello',
  (req, res) => {
    console.log('GET /apitechu/v1/hello');
    res.send('{"msg":"Hola desde API TechU"}');
  });

app.get('/apitechu/v1/users',
  (req, res) => {
    console.log('GET /apitechu/v1/users');
    const users = require('./usuarios.json');
    if (req.query.$count || req.query.$top) {
      console.log('options');
      const exit = {};
      if (req.query.$count) {
        console.log('$count');
        exit.Count = Object.keys(users).length;
      }
      if (req.query.$top && req.query.$top <= Object.keys(users).length && req.query.$top > 0) {
        console.log('$top');
        exit.Top = users.slice(0, req.query.$top);
      }
      res.send(exit);
    } else {
      console.log('else');
      res.send(users);
    }
  });

app.post('/apitechu/v1/users',
  (req, res) => {
    console.log('POST /apitechu/v1/users');
    console.log(req.headers.first_name);
    console.log(req.headers.last_name);
    console.log(req.headers.email);

    const users = require('./usuarios.json');

    const newUser = {
      id: users.length + 1,
      first_name: req.headers.first_name,
      last_name: req.headers.last_name,
      email: req.headers.email,
    };

    users.push(newUser);
    writeUserData2File(users);

    res.send(`{"Action": "create", "User": ${newUser.email}, "Status": "succuesfully saved"}`);
  });

app.delete('/apitechu/v1/users/:id',
  (req, res) => {
    console.log(`DELETE /apitechu/v1/users ID: ${req.params.id}`);
    const users = require('./usuarios.json');
    const user = users.find(o => o.id === parseInt(req.params.id, 10));
    users.splice(users.indexOf(user), 1);
    writeUserData2File(users);
    console.log(`Usuario ${JSON.stringify(user)} borrado`);
    res.send(`{Action: delete, User: ${JSON.stringify(user)}, "Status": "succuesfully deleted"}`);
  });

app.post('/apitechu/v1/monstruo/:p1/:p2',
  (req, res) => {
    console.log('POST /apitechu/v1/monstruo/:p1/:p2');
    console.log(`Parametres: ${JSON.stringify(req.params)}`);
    console.log(`Query String: ${JSON.stringify(req.query)}`);
    console.log(`Headers: ${JSON.stringify(req.headers)}`);
    console.log(`Body: ${JSON.stringify(req.body)}`);
    res.send(null);
  });

/**
 * Save user data into file system as JSON file usuarios.jsonx .
 * @param {array} data - array of users to be saved
 * @returns {None} Nothing to returns
 */
function writeUserData2File(data) {
  const fs = require('fs');

  fs.writeFile('./usuarios.json', JSON.stringify(data, null, 4), 'utf-8',
    (err) => {
      if (err) {
        console.log(err);
      } else {
        console.log('Usuarios persistido en fichero');
      }
    });
}
